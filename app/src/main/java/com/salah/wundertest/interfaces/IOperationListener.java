package com.salah.wundertest.interfaces;

import android.content.Context;

import com.salah.wundertest.model.api.response.BaseResponse;

/**
 * Created by salah on 6/12/18.
 */

public interface IOperationListener {

    void onOperationCompleted(Context context, BaseResponse response);

}
