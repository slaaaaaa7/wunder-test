package com.salah.wundertest.controller;

/**
 * Created by salah on 6/13/18.
 */

public class AppConfigs {

    public static final double p1Lat = 53.694865;
    public static final double p1Lon = 9.757589;
    public static final double p2Lat = 53.394655;
    public static final double p2Lon = 10.099891;
    public static final float map_zoom = 14.0f;

    public static final long MIN_TIME = 1000;
    public static final float MIN_DISTANCE = 0;

}
