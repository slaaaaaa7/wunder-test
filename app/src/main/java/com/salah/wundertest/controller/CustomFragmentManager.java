package com.salah.wundertest.controller;

import android.support.v4.app.FragmentManager;

import com.salah.wundertest.view.fragments.BaseFragment;
import com.salah.wundertest.view.activity.MainActivity;
import com.salah.wundertest.R;

/**
 * Created by salah on 6/12/18.
 */

public class CustomFragmentManager {

    private BaseFragment mCurrentFragment;
    private MainActivity mainActivity;
    private static CustomFragmentManager customFragmentManager;

    public void initializeFragmentManager(BaseFragment mCurrentFragment, MainActivity mainActivity) {
        this.mCurrentFragment = mCurrentFragment;
        this.mainActivity = mainActivity;
    }

    public static CustomFragmentManager getInstance() {
        if (customFragmentManager == null)
            customFragmentManager = new CustomFragmentManager();
        return customFragmentManager;
    }

    public void updateFragment(BaseFragment mCurrentFragment) {
        if (mCurrentFragment != null) {
            this.mCurrentFragment = mCurrentFragment;
            FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.main_frame, mCurrentFragment).addToBackStack(null).commit();
        }
    }
}
