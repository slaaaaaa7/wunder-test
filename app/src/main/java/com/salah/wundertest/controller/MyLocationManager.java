package com.salah.wundertest.controller;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;


/**
 * Created by salah on 9/13/18.
 */

public class MyLocationManager implements LocationListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private double currentLatitude;
    private static MyLocationManager myLocationManager;
    private double currentLongitude;
    private Context context;

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    private MyLocationManager() {

    }

    public static MyLocationManager getInstance(Context context) {
        if (myLocationManager == null) {
            myLocationManager = new MyLocationManager();
        }
        myLocationManager.context = context;
        return myLocationManager;
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
    }

    public void getLocation() {
        LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        boolean isGPSEnabled = false;

        boolean isNetworkEnabled = false;

        if (mLocationManager != null) {
            // getting GPS status
            isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }
        if (checkLocationPermission((Activity) context)) {
            if (!isGPSEnabled && !isNetworkEnabled) {
                Toast.makeText(context, "Cant find location Please Check GPS", Toast.LENGTH_LONG).show();
            } else {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, AppConfigs.MIN_TIME, AppConfigs.MIN_DISTANCE, this);
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, AppConfigs.MIN_TIME, AppConfigs.MIN_DISTANCE, this);
            }
        }
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(context, "Cant find location Please Check GPS", Toast.LENGTH_LONG).show();
    }

    public double getCurrentLatitude() {
        return currentLatitude;
    }

    public double getCurrentLongitude() {
        return currentLongitude;
    }


    public static boolean checkLocationPermission(final Activity activity) {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(activity)
                        .setTitle("Location Permission")
                        .setMessage("Please Allow  Location Permission")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(activity,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        //Request location updates:
                        getLocation();
                    }
                }
            }
        }
    }
}
