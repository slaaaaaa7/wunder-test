package com.salah.wundertest.view.activity;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;

import com.salah.wundertest.controller.CustomFragmentManager;
import com.salah.wundertest.controller.MyLocationManager;
import com.salah.wundertest.view.fragments.BaseFragment;
import com.salah.wundertest.view.fragments.ListFragment;
import com.salah.wundertest.R;
import com.salah.wundertest.view.fragments.MapFragment;
import com.salah.wundertest.interfaces.IOperationListener;
import com.salah.wundertest.model.CarModel;
import com.salah.wundertest.model.api.request.CarRequest;
import com.salah.wundertest.model.api.response.BaseResponse;
import com.salah.wundertest.model.api.response.CarResponse;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    private BaseFragment currentFragment;
    private BottomNavigationView mainNav;
    //Fragments
    private ListFragment listFragment;
    private MapFragment mapFragment;


    private ArrayList<CarModel> carModelArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initBottomNav();
        initializeLocationListener();
        CustomFragmentManager.getInstance().initializeFragmentManager(currentFragment, this);
        getCarsFromServer();
    }

    private void initializeLocationListener() {
        MyLocationManager.getInstance(this).getLocation();
    }

    private void initBottomNav() {
        mainNav = findViewById(R.id.bottom_nav);
        mainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                //get cars from server in case of the app couldn't retrieve them at the on create method
                getCarsFromServer();
                
                switch (item.getItemId()) {
                    case R.id.nav_list:
                        initListFragment();
                        return true;
                    case R.id.nav_map:
                        initMapFragment();
                        return true;
                }
                return false;
            }
        });
    }

    private void getCarsFromServer() {
        if (carModelArrayList == null)
            new CarRequest(this).getCars(this, new IOperationListener() {
                @Override
                public void onOperationCompleted(Context context, BaseResponse response) {
                    //populate the car list
                    carModelArrayList = ((CarResponse) response).getCarModelArrayList();
                    initListFragment();
                }
            });
    }

    private void initListFragment() {
        if (listFragment == null)
            listFragment = new ListFragment();

        //pass the cars to the list fragment
        passCars(listFragment);
        CustomFragmentManager.getInstance().updateFragment(listFragment);
    }

    private void initMapFragment() {
        if (mapFragment == null)
            mapFragment = new MapFragment();

        //pass the cars to the Map fragment
        passCars(mapFragment);
        CustomFragmentManager.getInstance().updateFragment(mapFragment);
    }

    private void passCars(BaseFragment fragment) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("cars", carModelArrayList);
        fragment.setArguments(bundle);
    }

    public void setCurrentFragment(BaseFragment currentFragment) {
        this.currentFragment = currentFragment;
    }

    public BaseFragment getCurrentFragment() {
        return currentFragment;
    }

    @Override
    public void onBackPressed() {

    }

}
