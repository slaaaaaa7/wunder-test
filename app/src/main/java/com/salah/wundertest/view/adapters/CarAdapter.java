package com.salah.wundertest.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.salah.wundertest.model.CarModel;
import com.salah.wundertest.R;

import java.util.ArrayList;

/**
 * Created by salah on 6/12/18.
 */

public class CarAdapter extends ArrayAdapter<CarModel> {

    private Context context;
    private int layoutResourceId;
    private ArrayList modelArrayList = null;

    public CarAdapter(Context context, int layoutResourceId, ArrayList<CarModel> modelArrayList) {
        super(context, layoutResourceId, modelArrayList);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.modelArrayList = modelArrayList;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            CarHolder holder = null;
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
                holder = initializeRowComponent(holder, convertView);
                convertView.setTag(holder);
            } else {
                holder = (CarHolder) convertView.getTag();
            }
            setCarData(holder, (CarModel) modelArrayList.get(position), convertView);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return convertView;
    }

    private CarHolder initializeRowComponent(CarHolder holder, View row) {
        holder = new CarHolder();
        holder.address = (TextView) row.findViewById(R.id.tv_address);
        holder.carCoordinate = (TextView) row.findViewById(R.id.tv_car_coordinate);
        holder.engineType = (TextView) row.findViewById(R.id.tv_engine_type);
        holder.exterior = (TextView) row.findViewById(R.id.tv_exterior);
        holder.fuel = (TextView) row.findViewById(R.id.tv_fuel);
        holder.interior = (TextView) row.findViewById(R.id.tv_interior);
        holder.name = (TextView) row.findViewById(R.id.tv_name);
        holder.vin = (TextView) row.findViewById(R.id.tv_vin);
        return holder;
    }

    private void setCarData(final CarHolder holder, CarModel car, View row) {
        holder.address.setText(String.valueOf(car.getAddress()));
        String coordinate = String.format("Lon : %s  /  Lat : %s", car.getCoordinates().get(0), car.getCoordinates().get(1));
        holder.carCoordinate.setText(coordinate);
        holder.engineType.setText(car.getEngineType());
        holder.exterior.setText(String.valueOf(car.getExterior()));
        holder.fuel.setText(String.valueOf(car.getFuel()));
        holder.interior.setText(car.getInterior());
        holder.name.setText(car.getName());
        holder.vin.setText(car.getVin());
    }

    private static class CarHolder {
        TextView address;
        TextView carCoordinate;
        TextView engineType;
        TextView exterior;
        TextView fuel;
        TextView interior;
        TextView name;
        TextView vin;
    }
}
