package com.salah.wundertest.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.salah.wundertest.view.activity.MainActivity;
import com.salah.wundertest.model.CarModel;
import com.salah.wundertest.R;
import com.salah.wundertest.view.CustomMapView;

import java.util.ArrayList;

/**
 * Created by salah on 6/12/18.
 */

public class MapFragment extends BaseFragment {

    private CustomMapView mapView;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_map;
    }

    @Override
    protected void initializeUIComponents(View view, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setCurrentFragment(this);
        mapView = (CustomMapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
    }

    @Override
    protected void initializeUIComponentsData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            final ArrayList<CarModel> carModelArrayList = (ArrayList<CarModel>) bundle.getSerializable("cars");
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    //set the google map variable on the custom class
                    mapView.setWunderGoogleMap(googleMap);
                    //Draw Cars On The Map
                    if (carModelArrayList != null)
                        mapView.drawCarsOnMap(carModelArrayList);
                }
            });
        }
    }

    @Override
    protected void initializeUIComponentsAction() {

    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }
}
