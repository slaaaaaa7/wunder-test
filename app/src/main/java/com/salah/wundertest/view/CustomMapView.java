package com.salah.wundertest.view;

import android.Manifest;
import android.animation.TypeEvaluator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.salah.wundertest.controller.AppConfigs;
import com.salah.wundertest.controller.MyLocationManager;
import com.salah.wundertest.model.CarModel;
import com.salah.wundertest.R;

import java.util.ArrayList;

/**
 * Created by salah on 6/12/18.
 */

public class CustomMapView extends MapView implements GoogleMap.OnCameraMoveStartedListener {

    private GoogleMap wunderGoogleMap;

    private ArrayList<Marker> carMarkers;

    private boolean isNameShown = false;

    public CustomMapView(Context context) {
        super(context);
        initializeMapView();
    }

    public CustomMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initializeMapView();

    }

    public CustomMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initializeMapView();
    }

    public CustomMapView(Context context, GoogleMapOptions googleMapOptions) {
        super(context, googleMapOptions);
        initializeMapView();
    }

    private void initializeMapView() {
        LayoutInflater.from(getContext()).inflate(R.layout.map_view, this, true);
        getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.getUiSettings().setTiltGesturesEnabled(false);
                googleMap.getUiSettings().setRotateGesturesEnabled(false);
                googleMap.setTrafficEnabled(false);
                googleMap.setMyLocationEnabled(true);


                //Create the bounds builder and include hamburg bounds in it.
                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                boundsBuilder.include(new LatLng(AppConfigs.p1Lat, AppConfigs.p1Lon));
                boundsBuilder.include(new LatLng(AppConfigs.p2Lat, AppConfigs.p2Lon));
                //build
                LatLngBounds latLngBounds = boundsBuilder.build();

                //tell the map object to move to this bounds
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        if (!isNameShown) {
                            marker.showInfoWindow();
                            hideMarkers(marker);
                            isNameShown = true;
                        } else {
                            marker.hideInfoWindow();
                            showMarkers();
                            isNameShown = false;
                        }
                        return true;
                    }
                });
            }
        });
    }

    public void drawCarsOnMap(ArrayList<CarModel> cars) {
        carMarkers = new ArrayList<>();
        for (int i = 0; i < cars.size(); i++) {
            CarModel car = cars.get(i);
            addCarMarkerLocation(car, car.getCoordinates().get(0), car.getCoordinates().get(1));
        }
    }

    public void hideMarkers(Marker marker) {
        for (int i = 0; i < carMarkers.size(); i++) {
            if (!marker.getTitle().equals(carMarkers.get(i).getTitle()))
                carMarkers.get(i).setVisible(false);
        }
    }

    public void showMarkers() {
        for (int i = 0; i < carMarkers.size(); i++) {
            carMarkers.get(i).setVisible(true);
        }
    }

    private void addCarMarkerLocation(CarModel car, double longitude, double latitude) {
        try {
            LatLng latLng = new LatLng(latitude, longitude);
            MarkerOptions carMarkerOption = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon))
                    .anchor(0.0f, 1.0f)
                    .title(car.getName())
                    .position(latLng);

            if (wunderGoogleMap != null) {
                carMarkers.add(wunderGoogleMap.addMarker(carMarkerOption));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public GoogleMap getWunderGoogleMap() {
        return wunderGoogleMap;
    }

    public void setWunderGoogleMap(GoogleMap wunderGoogleMap) {
        this.wunderGoogleMap = wunderGoogleMap;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    @Override
    public void onCameraMoveStarted(int i) {

    }

}
