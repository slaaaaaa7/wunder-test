package com.salah.wundertest.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;

import com.salah.wundertest.view.activity.MainActivity;
import com.salah.wundertest.model.CarModel;
import com.salah.wundertest.R;
import com.salah.wundertest.view.adapters.CarAdapter;

import java.util.ArrayList;

/**
 * Created by salah on 6/12/18.
 */

public class ListFragment extends BaseFragment {

    private ListView carsListView;
    private CarAdapter carAdapter;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_list;
    }

    @Override
    protected void initializeUIComponents(View view, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setCurrentFragment(this);
        carsListView = (ListView) view.findViewById(R.id.lv_cars);
    }

    @Override
    protected void initializeUIComponentsData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            ArrayList<CarModel> carModelArrayList = (ArrayList<CarModel>) bundle.getSerializable("cars");
            if (carModelArrayList != null) {

                //create the adapter
                if (carAdapter == null)
                    carAdapter = new CarAdapter(getContext(), R.layout.item_car_info, carModelArrayList);

                //set the adapter to the list view
                carsListView.setAdapter(carAdapter);
            }
        }
    }

    @Override
    protected void initializeUIComponentsAction() {

    }
}
