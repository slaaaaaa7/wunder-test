package com.salah.wundertest.model.api.response;

import com.google.gson.Gson;

/**
 * Created by salah on 6/12/18.
 */

public class BaseResponse {

    private String error;

    private boolean success;

    private static Gson gson = new Gson();

    public static <RS extends BaseResponse> RS parse(Class responseType, String responseString) {
        return (RS) gson.fromJson(responseString, responseType);
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
