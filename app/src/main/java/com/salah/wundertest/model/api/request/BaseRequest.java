package com.salah.wundertest.model.api.request;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.salah.wundertest.controller.VolleySingleton;
import com.salah.wundertest.view.activity.BaseActivity;
import com.salah.wundertest.interfaces.IOperationListener;
import com.salah.wundertest.model.api.response.BaseResponse;


/**
 * Created by salah on 6/12/18.
 */

public abstract class BaseRequest<RS extends BaseResponse> {

    private static final String baseUrl = "https://s3-us-west-2.amazonaws.com/wunderbucket/";

    private Context context;
    private Class<RS> responseType;
    private int methodType;


    BaseRequest(Context context, Class<RS> responseType, int methodType) {
        this.setContext(context);
        this.setResponseType(responseType);
        this.setMethodType(methodType);
        //TODO set default values for the base request
    }

    void invoke(final Context context, String apiName, final IOperationListener operationUIListener) {
        ((BaseActivity) context).showProgressDialog();

        StringRequest strReq = new StringRequest(getMethodType(), baseUrl + apiName, new Response.Listener<String>() {
            RS genericResponse = null;

            @Override
            public void onResponse(String response) {
                genericResponse = BaseResponse.parse(getResponseType(), response);
                genericResponse.setSuccess(true);
                if (operationUIListener != null) {
                    operationUIListener.onOperationCompleted(context, genericResponse);
                }
                ((BaseActivity) context).hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
                ((BaseActivity) context).hideProgressDialog();
            }
        }) {

        };
        // Adding request to request queue
        VolleySingleton.getInstance(context).addToRequestQueue(strReq);
    }


    private Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Class<RS> getResponseType() {
        return responseType;
    }

    public void setResponseType(Class<RS> responseType) {
        this.responseType = responseType;
    }

    public int getMethodType() {
        return methodType;
    }

    public void setMethodType(int methodType) {
        this.methodType = methodType;
    }

}
