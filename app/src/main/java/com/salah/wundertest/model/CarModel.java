package com.salah.wundertest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by salah on 6/12/18.
 */

public class CarModel implements Serializable {

    @Expose
    @SerializedName("address")
    private String address;

    @Expose
    @SerializedName("coordinates")
    private ArrayList<Double> coordinates;

    @Expose
    @SerializedName("engineType")
    private String engineType;

    @Expose
    @SerializedName("exterior")
    private String exterior;

    @Expose
    @SerializedName("fuel")
    private int fuel;

    @Expose
    @SerializedName("interior")
    private String interior;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("vin")
    private String vin;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    public String getExterior() {
        return exterior;
    }

    public void setExterior(String exterior) {
        this.exterior = exterior;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
