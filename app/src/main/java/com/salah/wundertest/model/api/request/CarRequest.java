package com.salah.wundertest.model.api.request;

import android.content.Context;

import com.android.volley.Request;
import com.salah.wundertest.interfaces.IOperationListener;
import com.salah.wundertest.model.api.response.CarResponse;

/**
 * Created by salah on 6/12/18.
 */

public class CarRequest extends BaseRequest<CarResponse> {

    private final String apiName = "locations.json";

    public CarRequest(Context context) {
        super(context, CarResponse.class, Request.Method.GET);
    }

    public void getCars(Context context, IOperationListener iOperationListener) {
        this.invoke(context, apiName, iOperationListener);
    }
}
