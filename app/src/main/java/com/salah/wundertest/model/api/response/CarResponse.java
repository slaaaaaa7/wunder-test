package com.salah.wundertest.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.salah.wundertest.model.CarModel;

import java.util.ArrayList;

/**
 * Created by salah on 6/12/18.
 */

public class CarResponse extends BaseResponse {

    @Expose
    @SerializedName("placemarks")
    private ArrayList<CarModel> carModelArrayList;

    public ArrayList<CarModel> getCarModelArrayList() {
        return carModelArrayList;
    }

    public void setCarModelArrayList(ArrayList<CarModel> carModelArrayList) {
        this.carModelArrayList = carModelArrayList;
    }
}
